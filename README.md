# 课程教学直播PC网站前端

#### 介绍
这是一个基于VUE框架的PC大网站平台。该平台具有丰富的课程资源，有专业的教师团队为您在线直播授课答疑。

#### 软件架构
软件架构说明

环境： node.js npm
VUE框架，es6.0/Sass语法

#### 安装教程

#####首次部署

1.  git init 初始化git
2.  git pull https://gitee.com/cloudhere/liveBroadcast.git 拉取代码
3.  npm install 下载相关依赖
4.  npm run build 打包文件-〉生成dist静态文件
5.  cd dist
6.  将dist内文件上传至nginx/dist目录下
7.  nginx -s reload

#####后续部署步骤

1.  git pull
2.  npm install 下载相关依赖
3.  npm run build 打包文件-〉生成dist静态文件
4.  将dist文件上传至nginx/dist目录下
5.  访问：http://39.98.115.157/

##### 开发
1.  拉去代码：git pull https://gitee.com/cloudhere/liveBroadcast.git
2.  下载依赖：npm install
3.  启动服务：npm run serve
npm run mock 模拟接口

#### 架构树

|-- node_modules: # 存放下载依赖的文件夹
|-- public: # 存放不会变动静态的文件，它与src/assets的区别在于，public目录中的文件不被webpack打包处理，会原
样拷贝到dist目录下
　　|-- index.html: # 主页面文件
　　|-- favicon.ico: # 在浏览器上显示的图标
|-- src: # 源码文件夹
　　|-- assets: # 存放组件中的静态资源
　　|-- components: # 存放一些公共组件
　　|-- views: #  存放所有的路由组件
　　|-- App.vue: # 应用根主组件
　　|-- main.js: # 应用入口 js
|-- .browserslistrc: # 指定了项目可兼容的目标浏览器范围, 对应是package.json 的 browserslist选项
|-- .eslintrc.js: # eslint相关配置
|-- .gitignore: # git 版本管制忽略的配置
|-- babel.config.js: # babel 的配置,即ES6语法编译配置
|-- package-lock.json: # 用于记录当前状态下实际安装的各个包的具体来源和版本号等, 保证其他人在 npm install 项目时大家的依赖能保证一致.
|-- package.json: # 项目基本信息,包依赖配置信息等
|-- postcss.config.js: # postcss一种对css编译的工具，类似babel对js的处理
|-- README.md: # 项目描述说明的 readme 文件

#### 使用说明

1.  路由配置请见src/router
2.  相关指令请见src/plugins/directives
3.  相关组件请见src/components
4.  过滤器请见 src/plugins/vue-filter

#### 相关依赖

1.axios介绍：https://www.kancloud.cn/yunye/axios/234845
2.ant-design-vue: https://www.antdv.com/docs/vue/use-with-vue-cli-cn/

import Vue from 'vue';
import { DatePicker } from 'ant-design-vue';
Vue.use(DatePicker);
引入样式：

import 'ant-design-vue/dist/antd.css'; // or 'ant-design-vue/dist/antd.less'


