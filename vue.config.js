module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: '@import "@/scss/settings.scss";'
            }
        }
    },
    devServer: {
        port: 55001
    }
};
