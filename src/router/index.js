import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '../views/index.vue';
import AnswerBoard from '../views/questionBank/answerBoard/admin.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/answerBoard',
        name: 'answerBoard',
        component: AnswerBoard,
        children: [
            {
                path: 'test',
                name: 'test',
                component: () => import('../views/questionBank/answerBoard/index.vue')
            },
            {
                path: 'analysis',
                name: 'analysis',
                component: () => import('../views/questionBank/answerBoard/analysis/analysis')
            }
        ]
    },
    {
        path: '/',
        name: 'index',
        component: Index,
        children: [
            {
                path: 'about',
                name: 'about',
                component: () => import('../views/About.vue')
            },
            {
                path: 'download',
                name: 'download',
                component: () => import('../views/download.vue')
            },
            {
                path: 'register',
                name: 'register',
                component: () => import('../views/register.vue')
            },
            {
                path: 'login',
                name: 'login',
                component: () => import('../views/Login.vue')
            },
            {
                path: 'home',
                name: 'homepage',
                component: () => import('../views/home/index.vue')
            },
            {
                path: 'courseCenter',
                name: 'courseCenter',
                component: () => import('../views/courseCenter/index.vue')
            },
            {
                path: 'newActivity',
                name: 'newActivity',
                component: () => import('../views/courseCenter/newactivity.vue')
            },
            {
                path: 'practiceDelivery',
                name: 'practiceDelivery',
                component: () => import('../views/courseCenter/practiceDelivery.vue')
            },
            {
                path: 'freeLive',
                name: 'freeLive',
                component: () => import('../views/freeLive/index.vue')
            },
            {
                path: 'live',
                name: 'live',
                component: () => import('../views/freeLive/live.vue')
            },
            {
                path: 'video',
                name: 'video',
                component: () => import('../views/freeLive/video.vue')
            },
            {
                path: 'famousCourse',
                name: 'famousCourse',
                component: () => import('../views/famousCourse/index.vue')
            },
            {
                path: 'questionBank',
                name: 'questionBank',
                component: () => import('../views/questionBank/index.vue')
            },
            {
                path: 'littleClassroom',
                name: 'littleClassroom',
                component: () => import('../views/littleClassroom/index.vue')
            },
            {
                path: 'studyCenter',
                name: 'studyCenter',
                component: () => import('../views/studyCenter/index.vue')
            },
            {
                path: 'setting',
                name: 'setting',
                component: () => import('../views/setting/index.vue')
            },
            {
                path: 'landingPage',
                name: 'landingPage',
                component: () => import('../views/courseCenter/landingPage.vue')
            },
            {
                path: 'shoppingCar',
                name: 'shoppingCar',
                component: () => import('../views/courseCenter/shoppingCar.vue')
            },
            {
                path: 'ordering',
                name: 'ordering',
                component: () => import('../views/courseCenter/ordering.vue')
            },
            {
                path: 'order',
                name: 'order',
                component: () => import('../views/courseCenter/order.vue')
            },
            {
                path: 'paysucess',
                name: 'paysucess',
                component: () => import('../views/courseCenter/paysucess.vue')
            },
            {
                path: 'paysucesszfb',
                name: 'paysucesszfb',
                component: () => import('../views/courseCenter/paysucesszfb.vue')
            },
            {
                path: 'labIntro',
                name: 'labIntro',
                component: () => import('../views/courseCenter/labIntro.vue')
            },
            {
                path: '*',
                redirect: {
                    name: 'homepage'
                }
            },


        ]
    },
    {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(/* webpackChunkName: "about" */ '../views/About.vue')
    }

];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router;
