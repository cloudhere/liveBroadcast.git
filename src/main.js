import 'babel-polyfill'
import 'es6-promise/auto'
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Antd from 'ant-design-vue';
import './plugins/basePlugin.js';
import infiniteScroll from 'vue-infinite-scroll'
Vue.use(infiniteScroll)
Vue.config.productionTip = false;
Vue.use(Antd);
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
Vue.$router = router;
window.Vue = Vue;
