import Vue from 'vue';
import API from './api.js';
import utils from './utils.js';
import $ from 'jquery';

let _axios = window.axios;

const INTERFACE = {
    // 获取用户信息
    getAccount() {
        let account = {};
        let cookieAccount = JSON.parse(
            Vue.prototype.$util.getCookie('account') || 'null'
        );
        let phone;
        if (cookieAccount && cookieAccount.phone) {
            phone = cookieAccount.phone.slice(0, 3) + '***' + cookieAccount.phone.slice(-4)
        }
        Object.assign(account, cookieAccount);
        if (Vue.prototype.$util.getCookie('token')) {
            account.hidePhone = phone;
            account.name = account.name || phone || '游客'
        }
        if (!cookieAccount)
            Vue.prototype.$util.setCookie('account', JSON.stringify(account));
        return account;
    },
    // 请求接口
    /* 示例：
     *   this.$api.ajaxData({
     *       hostName: 'def',       // 主机地址
     *       pathName: 'getZsList', // 请求地址名 见api.js PATHS:{}
     *       forcePrd: false,       // 强制生产
     *       method: 'post',        // 请求方式
     *       authorization: false,  // 加token认证信息 必须要登录的接口
     *       errorFunc: true, // 错误提示处理
     *   })
     *       .then( res => {
     *           doSomeThing();
     *       })
     *
     * */
    ajaxData(option) {
        return getData(option);
    },
    // 用的较多的接口
    // 获取证书列表
    ajaxGetZsList() {
        let option = {
            pathName: 'getZsList'
        };
        return this.ajaxData(option);
    },
    // 退出登录
    exitUser() {
        Vue.prototype.$util.setCookie('account', '');
        Vue.prototype.$util.setCookie('token', '');
        Vue.$router.replace({name: 'login'});
    }
};

/**
 * 封装请求方法
 * @param {Object} opt 配置项
 *      {Boolean} authorization 授权
 */
function getData(opt) {
    let options = Object.assign({
        errorFunc: true
    }, opt);
    let postData = Object.assign({}, options.data);
    let token = Vue.prototype.$util.getCookie('token') || null;
    let dtd = $.Deferred();
    let userInfo = _api.getAccount();
    let headers = {
        // 'Access-Control-Allow-Origin': '*'
        token: token
    };
    // 接入token
    if (options.authorization) {
        headers = $.extend(headers, {
            token: token || undefined,
            Authorization: token ? 'Bearer ' + token : undefined
        });
        if (options.data) {
            postData = $.extend(postData, {
                phone: userInfo.phone
            })
        } else {
            options.params = $.extend(options.params, {
                phone: userInfo.phone
            });
        }
    }
    let setting = {
        url: API.getAjaxUrl(
            options.hostName,
            options.pathName,
            options.forcePrd
        ),
        data: postData,
        params: options.params,
        method: options.method || 'get', // 默认get请求，应为接口都只读取了params内的参数
        timeout: 10000,
        headers: headers
    };
    _axios(setting)
        .then(res => {
            console.log(setting.url, setting.url.indexOf('pcjk/Qq/getXwspCon_pc'));
            if (res.status === 200 && res.data.code === 200) {
                dtd.resolve(res.data);
            } else if (res.status === 200 && res.data.code === 500) {
                _api.exitUser();
            } else {
                if (res.status === 200 && res.data.code == 400) {
                    if (setting.url.indexOf('pcjk/Qq/getXwspCon_pc') > -1) {//如果是滚动加载更多的接口需要返回空值
                        dtd.resolve(res.data);
                    } else {
                        // 请求错误
                        if (options.errorFunc) {
                            resolveError(res.data.code, res.data.msg);
                        }
                        dtd.reject(res.data.msg);
                    }
                } else {
                    // 请求错误
                    if (options.errorFunc) {
                        resolveError(res.data.code, res.data.msg);
                    }
                    dtd.reject(res.data.msg);
                }

            }
        })
        .catch(res => {
            // 网络超时 服务宕机
            if (options.errorFunc) {
                resolveError(res.code, res.message);
            }
            dtd.reject(res.message);
        });
    return dtd.promise();
}

// 统一错误处理
function resolveError(code, msg) {
    if (code === 400) {
        utils.alert(msg);
    } else if (code === -1) {
        utils.alert(msg);
        console.log(code + ': ' + msg);
    } else {
        console.log(code + ': ' + msg);
    }
}

let _api = {
    ...INTERFACE,
    getAjaxUrl: API.getAjaxUrl,
    getAjaxHost: API.getAjaxHost
};

let Plugin_API = {
    install(Vue) {
        window._api = _api;
        Vue.$api = _api;
        Object.defineProperties(Vue.prototype, {
            $api: {
                get() {
                    return _api;
                }
            }
        });
    }
};
Vue.use(Plugin_API);

export default _api;
