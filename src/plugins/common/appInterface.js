(function() {
    var api = {
        commandQueue: [],
        cmdQueue: {},
        index: 0,
        mNativeFunction: {}
    };
    var protocol = 'beacon';
    api.os = function() {
        var ua = navigator.userAgent;
        var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
        var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
        var os = {};
        if (iphone) os.ios = os.iphone = true;
        if (ipad) os.ios = os.ipad = true;
        return { ios: os.ios };
    };
    api.exec = function(cmd, option, callback) {
        api.index += 1;
        var callbackId = new Date().getTime() + '|' + api.index;
        option = option || {};
        api.cmdQueue[callbackId] = callback;
        var nativeOption = {
            callbackId: callbackId,
            option: option
        };
        var apiContent =
            protocol + '://[' + cmd + '-' + JSON.stringify(nativeOption) + ']';
        prompt(protocol, apiContent);
        //}
    };

    api.pcExec = function(cmd, option, callback) {
        api.index += 1;
        var callbackId = new Date().getTime() + '|' + api.index;
        option = option || {};
        api.cmdQueue[callbackId] = callback;
        var nativeOption = {
            callbackId: callbackId,
            option: option
        };
        beaconApi.getAccountInfo(nativeOption);
    };
    window.AppCallback = function(callbackId, option) {
        //$.alert(callbackId);
        // $.alert(option);
        if (api.cmdQueue[callbackId]) {
            api.cmdQueue[callbackId](option);
        }
    };
    var beaconApi = (window[protocol + 'Api'] = api);
    //客户端预埋接口
    window.ReqWeb = function(key, value) {
        if (api.mNativeFunction[key] != undefined) {
            api.mNativeFunction[key](value);
        }
    };
    //监听native
    window.lisenNative = function(key, func) {
        api.mNativeFunction[key] = func;
    };
})();
(function() {
    var beaconApi = window.beaconApi;
    var NativeProxy = window.NativeProxy;
    var beacon = {};
    var isIos = beaconApi.os().ios;
    var from = getQueryString('dt_from');
    var source = getQueryString('source');
    function getQueryString(name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        var r = window.location.search.substr(1).match(reg);
        if (r != null) {
            var value = r[2];
            value = value.replace(/(%22|%3E|%3C|<|>)/g, 'MM');
            return decodeURIComponent(value);
        } else {
            return null;
        }
    }

    //web是app分享出去，nod是没有底部下拉框但有顶部硬logo，clean什么都不带，pcClient是pc客户端
    beacon.checkIfCanUseApi = function() {
        if (
            from == 'web' ||
            from == 'nod' ||
            from == 'clean' ||
            source == 'pcClient'
        ) {
            return false;
        } else {
            return true;
        }
    };
    beacon.getAccountInfo = function(option, callBack) {
        //给pc用的,并且必须在dt_from为web时才会进去
        if (!this.checkIfCanUseApi() && source != 'pcClient') {
            //新增iDLControlKey  代表后台控制2.0版本聊天室功能开启/隐藏，为了应付crm系统上线延期的最坏打算 1=开启  20181115
            var accountInfo = {
                AID: 0,
                GUID: '',
                DUA: '',
                ztIsInReview: 0,
                iDLControlKey: 1
            };
            callBack(accountInfo);
            return;
        }
        if (source == 'pcClient') {
            try {
                beaconApi.pcExec('getAccountInfo', option, function(data) {
                    //终端得到的字段转成后台统一字段，特别注意DUA需要encodeURIComponent
                    callBack({
                        AID: data.AID,
                        GUID: data.GUID,
                        // DUA: encodeURIComponent(data.DUA),
                        DUA: data.DUA,
                        ticket: data.TICKET,
                        dtCellphoneState: data.dtCellphoneState
                    });
                });
            } catch (e) {
                callBack({
                    AID: 18914,
                    GUID: '24CE31101326D9B13DEFCA5D9D8C893B',
                    DUA:
                        'SN=IOSCJPH182_GA&VN=182080222&BN=0&VC=APPLE&MO=iPhone&RL=375_667&CHID=1000&LCID=0&RV=&OS=9.3.2&DV=V1&status=0',
                    ticket: 'eQU2uxOVYDgcXhkSQyHC7XPMfVByIKuaP6PVtgbVrKY.',
                    dtCellphoneState: '',
                    dtnickname: '',
                    dtheadimgurl: ''
                });
            }
            // });
        } else if (isIos) {
            beaconApi.exec('getAccountInfo', option, function(data) {
                //终端得到的字段转成后台统一字段，特别注意DUA需要encodeURIComponent
                callBack({
                    AID: data.dtid,
                    GUID: data['DT-GUID'],
                    // DUA: encodeURIComponent(data['DT-UA']),
                    DUA: data['DT-UA'],
                    ticket: data.dtticket,
                    dtCellphoneState: data.dtCellphoneState,
                    dtnickname: data.dtnickname,
                    dtheadimgurl: data.dtheadimgurl,
                    dtMemberType: data.dtMemberType,
                    dtMemberEndTime: data.dtMemberEndTime,
                    iosChecking: data.iosChecking,
                    dtUserRealName: data.dtUserRealName,
                    dtUserIDNumber: data.dtUserIDNumber,
                    ztIsInReview: data.ztIsInReview,
                    accountProfile: data.accountProfile, //默认传入用户简介
                    UUID: data.uuid, //设备唯一标识
                    iDLControlKey: data.iDLControlKey,
                    token: data.token,
                    version: data.key_version_name // 版本号
                });
            });
        } else {
            try {
                let accountInfo = NativeProxy.getAccountInfo();
                //$.mobileConsole(accountInfo);
                if (typeof accountInfo != 'object') {
                    accountInfo = JSON.parse(accountInfo);
                }
            } catch (e) {
                // $.mobileConsole(e);
                accountInfo = {
                    // dtticket: "cetcHzmvf86UrXs1McH3PZvZY14scy67QMIWBCA7rW0.",
                    // dtid: 18899,
                    dtticket: null,
                    dtid: null,
                    'DT-GUID': '',
                    'DT-UA': '',
                    dtCellphoneState: 0,
                    dtnickname: '',
                    dtheadimgurl: '',
                    dtMemberType: '',
                    dtMemberEndTime: '',
                    dtUserRealName: '',
                    dtUserIDNumber: '',
                    accountProfile: '',
                    iDLControlKey: 0,
                    token: '',
                    version: ''
                };
            }
            if (!accountInfo.dtheadimgurl) accountInfo.dtheadimgurl = '';
            callBack({
                AID: accountInfo.dtid,
                GUID: accountInfo['DT-GUID'],
                // DUA: encodeURIComponent(accountInfo['DT-UA']),
                DUA: accountInfo['DT-UA'],
                IMEI: accountInfo.IMEI,
                ticket: accountInfo.dtticket,
                dtCellphoneState: accountInfo.dtCellphoneState,
                dtnickname: accountInfo.dtnickname,
                dtheadimgurl: accountInfo.dtheadimgurl,
                dtMemberType: accountInfo.dtMemberType,
                dtMemberEndTime: accountInfo.dtMemberEndTime,
                dtUserRealName: accountInfo.dtUserRealName,
                dtUserIDNumber: accountInfo.dtUserIDNumber,
                accountProfile: accountInfo.accountProfile,
                UUID: accountInfo.uuid, //设备唯一标识
                iDLControlKey: accountInfo.iDLControlKey,
                token: accountInfo.token,
                version: accountInfo.key_version_name // 版本号
            });
        }
    };
    //回退直接关闭
    beacon.setDirectBackButtonListenerEnable = function(option, callBack) {
        if (!this.checkIfCanUseApi()) {
            return;
        }
        if (isIos) {
            beaconApi.exec(
                'setDirectBackButtonListenerEnable',
                option,
                function(data) {
                    callBack(data);
                }
            );
        } else {
            try {
                NativeProxy.setDirectBackButtonListenerEnable(
                    JSON.stringify(option)
                );
            } catch (e) {}
            callBack({});
        }
    };

    window.beacon = beacon;
})();
