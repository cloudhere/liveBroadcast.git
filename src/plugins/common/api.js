const API = {
    HOSTS: {
        loc: `//${location.host}/`,
        // ******>> 3环境配置 <<******
        // 默认请求配置 - 默认java
        'dev.def': '//39.98.115.157:888/',
        'prd.def': '//39.98.115.157:888/'
    },
    /*
     * 获取请求or跳转前缀域名
     * @param {String} hostName 类型
     * @param {Boolean} forcePrd 强制走生产
     */
    getAjaxHost(hostName, forcePrd) {
        let nowUrl = location.href;
        let protocol = location.protocol;
        let env = ''; // 环境
        let HOSTS = this.HOSTS;
        let host = '';
        // 1.判断环境
        if (/localhost/.test(nowUrl)) {
            //开发环境
            env = 'dev';
        } else {
            env = 'prd';
        }
        // forcePrd默认false；如果填了则强制走正式数据
        env = forcePrd ? 'prd' : env;
        // 针对特别情况，走本地跳转需要
        if (hostName === 'local') return protocol + HOSTS.loc;

        // 2.获取对应host
        /**
         * 规则：
         * 1、dt服务所有环境一样，直连prd
         * 2、dev 与 sit,prd 跳转相关的表现不一致，这是由于部署方式不一致导致；
         *      - dev,本地开发环境一致;
         *        sit,prd是微服务
         *      - dev所有都走47.99.136.159/dev.gushi.com
         *        sit,prd根据不同微服务走不同的地址如: //47.97.111.88:33332/ [sit.gushi.com/sec]
         * 3、[[跳转和接口前缀区分设置]]：
         *      - news 接口 || newsHref 跳转
         * 4、重重重点：：：后续新页面开发优先使用对应域名的第一个case的值
         *      - 如使用 news系列，  hostName 优先使用news
         */
        switch (hostName) {
            case 'mock':
                host = HOSTS[env + '.mock'];
                break;
            // ****** 默认请求地址 ******  [payRead, testGushi,zeroActivity]
            default:
                host = HOSTS[env + '.def'];
        }
        return /^http/.test(host) ? host : protocol + host;
    },
    /**
     * 获取请求路径
     * @param {String} name 名称
     */
    getAjaxPath(pathName) {
        // let nowUrl = location.href;
        // process.env.NODE_ENV
        // if (/localhost/.test(nowUrl)) {
        //     return pathName;
        // } else {
        return API.PATHS[pathName] || '';
        // }
    },
    /**
     * 获取请求路径
     * @param {String} hostName host类型
     * @param {String} pathName path名称
     * @param {Boolean} forcePrd
     */
    getAjaxUrl(hostName, pathName, forcePrd) {
        let host = API.getAjaxHost(hostName, forcePrd);
        let path = API.getAjaxPath(pathName);
        if (!path) {
            // console.log('获取请求路径出错');
        }
        return host + path;
    },
    PATHS: {
        login: 'pcjk/grzx/doRegister', // 1.登录
        getYzm: 'pcjk/grzx/getYzm', // 2.完善信息 获取验证码
        doPerfect: 'pcjk/grzx/doPerfect_pc', // 3.信息完善
        loginByUser: 'pcjk/grzx/doLoginByUser', // 4.密码登录
        upLoadImg: 'pcjk/grzx/upLoadImg', //5.牛上传图片（pc，手机端那边自行集成）
        getGrzl: 'pcjk/grzx/getGrzl', // 8.个人信息查询
        doUpUserInfo: 'pcjk/grzx/doUpUserInfo_pc', //9.个人信息修改（包括密码修改，不用单独页面功能）
        doGetPwd:'pcjk/grzx/doGetPwd',//7.忘记密码
        getLoginYzm: 'pcjk/grzx/doGetYzm', //8.登录获取验证码
        loginByYzm: 'pcjk/grzx/doLoginByYzm', //9.验证码登录
        doGetYzm3:'pcjk/grzx/doGetYzm3',//11.修改手机号-获取验证码
        doUpPhone:'pcjk/grzx/doUpPhone',//11.修改手机号-获取验证码
        doUpdatePwd: 'pcjk/grzx/doUpdatePwd', //18.修改密码
        doCheckHb: 'pcjk/tool/doCheckHb', // 20.直播验签心跳
        doGetFreeLive: 'pcjk/sy/doGetFreeLive', //24．获取热门直播（PC那边免费直播改成热门直播）
        getZsList: 'pcjk/sy/doGetZsList', // 25.获取证书列表
        getGgList: 'pcjk/sy/doGgList', // 26.获取首页广告图
        doGetTjCourses: 'pcjk/sy/doGetTjCourses', //27.获取证书推荐课程（PC）
        doGgZsBqs: 'pcjk/sy/doGgZsBqs', // 28.获取证书标签信息
        doGetGoods: 'pcjk/sy/doGetGoods', //29.获取商品信息-首页-课程
        doGetGoodsF:'pcjk/sy/doGetGoodsF',//30.首页-财鑫商院-四大学院（pc）
        doGetHdF:'pcjk/sy/doGetHdF',//31.首页-财鑫商院-最鑫活动
        doGetSsF:'pcjk/sy/doGetSsF',//32.首页-财鑫商院-实习速递
        doGetGoodsJF:'pcjk/sy/doGetGoodsJF',//33.获取教辅信息
        doGetZixun:'pcjk/sy/doGetZixun',//34.获取资讯信息（pc）
        doGetMryl:'pcjk/sy/doGetMryl',//35.每日一练(pc)
        doGetFLabs: 'pcjk/Qq/doGetFLabs', //36.获取一级科目标签
        doGetSLabs: 'pcjk/Qq/doGetSLabs', //37.获取二级科目标签
        doLiveYuYue: 'pcjk/sy/doLiveYuYue', //38.直播预约
        getNewActivityList:'pcjk/sy/doGetAllHdF',//39.最鑫活动
        doApplyHd: 'pcjk/sy/doApplyHd',// 40.最鑫活动-申请
        doGetDelivery:'pcjk/sy/doGetAllSsF',//41.更多实习速递列表
        doGetGoodsXk: 'pcjk/Qq/doGetGoodsXk', // 42.选课中心
        doGetAPPMsst:'/pcjk/msst/doGetAPPMsst',//43.名师试听/课堂（PC手机菜单名不一样）
        doGetPCMsstGood: 'pcjk/msst/doGetPCMsstGood', //44.名师试听/课堂-试听课链接
        doGetCarList: 'pcjk/shopcar/doGetCarList', //45.查询购物车信息
        doGetCarDel: 'pcjk/shopcar/doDelShopCar', //46.购物车删除
        doAddShopCar: 'pcjk/shopcar/doAddShopCar', //47.加入购物车
        doGetGoodsById: 'pcjk/Qq/doGetGoodsById', // 48.商品详情页
        initFky: 'pcjk/Qq/initFky', // 49.订单页初始化
        doGetAddress: 'pcjk/grzx/doGetAddress', // 50.地址列表获取
        doAddAddress: 'pcjk/grzx/doAddAddress_pc', // 52.地址新增
        doModifyAddress: 'pcjk/grzx/doModifyAddress_pc', // 54.地址修改
        doDelAddress: 'pcjk/grzx/doDelAddress', // 55.地址删除
        doGetFreeLives: 'pcjk/Qq/doGetFreeLives', //53.免费直播-最近和往期
        doGetFreeLivesApp: 'pcjk/Qq/doGetFreeLives_app', //54.免费直播-最近和往期
        getXwspZx:'pcjk/Qq/getXwspZx',//55.小微课堂-最新4个（pc）
        getXwspCon:'pcjk/Qq/getXwspCon_pc',//57.小微课堂-最新4个（pc）
        doCheckSm:'pcjk/tool/doCheckSm',//62.是否实名校验
        doSmdj:'pcjk/tool/doSmdj_pc',//64.实名校验-pc
        doGetPlayAuth: 'pcjk/tool/doGetPlayAuth', //65.获取播放凭证
        doGetBfAddress: 'pcjk/tool/doGetBfAddress', // 66.获取播放地址（切换清晰的）
        doGetZfbInfo: 'pcjk/tool/doGetZfbInfo_pc', //78.支付宝支付接口（PC）
        signVerified: 'pcjk/tool/signVerified', //79.支付宝验签接口（PC）
        doGetVxpay: 'pcjk/tool/doGetVxpay_pc', // 80.微信支付获取支付二维码链接接口（PC）
        getTodayLiveList: 'pcjk/grzx/getTodayLiveList', // 81.个人中心获取今日直播
        getCourseList: 'pcjk/grzx/getCourseList', // 82.获取用户课程
        getCourseChaInfoList: 'pcjk/grzx/getCourseChaInfoList',//83.个人中心-根据课程id获取章信息
        getLiveListByCourseId: 'pcjk/grzx/getLiveListByCourseId', //85.个人中心-根据直播课id查看直播列表信息
        getVideoListByChaId: 'pcjk/grzx/getVideoListByChaId',//86.个人中心-根据课程下的章id查看关联的录播视频列表
        getOrderList:'pcjk/order/doGetAllOrder', //87.个人中心-订单管理，查看全部订单列表
        doGetYzfOrder: 'pcjk/order/doGetYzfOrder', //88.个人中心-订单管理，查看已付款
        doGetDzfOrder: 'pcjk/order/doGetDzfOrder', //89.个人中心-订单管理，查看待付款
        doGetYqxOrder: 'pcjk/order/doGetYqxOrder', //90.个人中心-订单管理，查看已取消
        doCancelOrder: 'pcjk/order/doCancelOrder', //91.个人中心-订单管理，取消订单
        doApplyFaPiao: 'pcjk/order/doApplyFaPiao_pc', //93.个人中心-订单管理，申请发票
        doApplyRebund: 'pcjk/order/doApplyRebund_pc', //95.个人中心-订单管理，申请退款
        saveOrder: 'pcjk/order/saveOrder', // 97.提交订单
        getOrderListDetail:'pcjk/order/doGetOrderDetail',//100.获取支付详情
        doGetOrderDetail: 'pcjk/order/doGetOrderDetail_pc', //100.获取订单详情_pc
        doGetVxres: 'pcjk/tool/doGetVxres', // 102.微信验签接口
        getKjUrl: 'pcjk/grzx/getKjUrl', //103.点播课-根据章ID刷新获取课件信息
        saveSplog: 'pcjk/tool/saveSplog', //视频日志
        doGetLiveInfo: 'pcjk/sy/doGetLiveInfo',
        //题库新
        doGetSLabsById: 'pcjk/Qq/doGetSLabsByFid', // 2.根据一级目录获取二级目录
        doFreeQuestionList: 'pcjk/tiku/doFreeQuestionList_pc',//67.题库-获取免费题目
        doHisTrueTestList: 'pcjk/tiku/doHisTrueTestList_pc',//4.题库-获取历年真题试卷列表
        doEnterTest:'pcjk/tiku/doEnterTest_pc',//获取历年试卷下的真题
        doCreateZhiNengTest:'pcjk/tiku/doCreateZhiNengTest_pc',//智能选组
        getCourseListPC: 'pcjk/tiku/getCourseList_pc', // 5.获取用户课程
        doChpTestList: 'pcjk/tiku/doChpTestList_pc', // 7.题库-获取章节试卷列表
        doGetTestLogList: 'pcjk/tiku/doGetTestLogList_pc',//13.题库-查看做试卷历史
        doSubAnser: 'pcjk/tiku/doSubAnser_pc',//题库-提交单个题目的答案
        doSubmitTest:'pcjk/tiku/doSubmitTest_pc',//题库-试卷提交
        getErrorQuesList_pc:'pcjk/tiku/getErrorQuesList_pc',//查看错题集
        doQueryTestReport:'pcjk/tiku/doQueryTestReport_pc',//查看试卷报告
        deleteErrorQues: 'pcjk/tiku/deleteErrorQues_pc' // 删除错题

    }
};

export default API;
