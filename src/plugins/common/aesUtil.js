// import Vue from 'vue';
import CryptoJS from 'crypto-js';
let keyStr = 'Cxwx2020_quxi-an';
export default {
    //加密
    encrypt(word) {
        var key = CryptoJS.enc.Utf8.parse(keyStr); //Latin1 w8m31+Yy/Nw6thPsMpO5fg==
        var srcs = CryptoJS.enc.Utf8.parse(word);
        var encrypted = CryptoJS.AES.encrypt(srcs, key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    },
    //解密
    decrypt(word,decryptKey) {
        let ikey = decryptKey || keyStr
        var key = CryptoJS.enc.Utf8.parse(ikey); //Latin1 w8m31+Yy/Nw6thPsMpO5fg==
        var decrypt = CryptoJS.AES.decrypt(word, key, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        let value = CryptoJS.enc.Utf8.stringify(decrypt).toString();
        return value;
    }
};
