#公共方法说明

## ajax请求

// 请求接口
/* 示例：
 *   this.$api.ajaxData({
 *       hostName: 'def',       // 主机地址
 *       pathName: 'getZsList', // 请求地址名 见api.js PATHS:{}
 *       forcePrd: false,       // 强制生产
 *       method: 'post',        // 请求方式
 *       authorization: false,  // 加token认证信息 必须要登录的接口
 *
 *   })
 *       .then( res => {
 *           doSomeThing();
 *       })
 */

##请求用户信息
this.$api.getAccount();

##toast提示
    this.$util.alert(tips, times)