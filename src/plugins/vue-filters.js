// vue 过滤器
import Vue from 'vue';

const filters = {
    /*
     * Empty Value Fill Default Value
     *
     * @param {string} val
     * @param {string} defaultValue 默认值
     * return string val
     * */
    emptyValueFill: function(val, defaultValue = '--') {
        return val || defaultValue;
    },
    /*
     * Format Data Time
     *
     * @param {string} val
     * @param {string} fmt  格式化类型默认yyyy-MM-dd
     * @param {boolean} transQuarter  转化季度
     * @param {Object} transQuarterRule  转化季度规则
     * return fmt
     * */
    formatDateTime: function(
        val,
        fmt = 'yyyy-MM-dd',
        transQuarter = false,
        transQuarterRule
    ) {
        let value = val || '';
        if (!value) return '';
        // 时间戳
        if (!isNaN(value)) {
            let str = value.toString();
            value = Number((str + '0000000000000').slice(0, 13));
            value = Number(value);
        }
        // IOS 需要转成yyyy/MM/dd
        else
            value = value
                .toString()
                .replace(/-/g, '/')
                .replace(/\..$/g, '');
        let date = new Date(value);
        if (!date) return value;
        // 时间格式化
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(
                RegExp.$1,
                (date.getFullYear() + '').substr(4 - RegExp.$1.length)
            );
        } else {
            return '';
        }
        let o = {
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'h+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds()
        };
        // 转化季度
        if (transQuarter && transQuarterRule) {
            for (let t in transQuarterRule) {
                let str =
                    (o['M+'] >= 10 ? o['M+'] : ('00' + o['M+']).substr(-2)) +
                    '-' +
                    (o['d+'] >= 10 ? o['d+'] : ('00' + o['d+']).substr(-2));
                let mt = t.split('-')[0];
                let dt = t.split('-')[1];
                let strRule =
                    (mt.length === 1 ? ('00' + mt).substr(-2) : mt) +
                    '-' +
                    (dt.length === 1 ? ('00' + dt).substr(-2) : dt);
                if (str === strRule) {
                    return date.getFullYear() + '' + transQuarterRule[t];
                }
            }
        }
        for (let k in o) {
            if (new RegExp(`(${k})`).test(fmt)) {
                let str = o[k] + '';
                fmt = fmt.replace(
                    RegExp.$1,
                    RegExp.$1.length === 1
                        ? str
                        : ('00' + str).substr(str.length)
                );
            }
        }
        return fmt;
    },
    /*
     * Transform Number to 0.**
     *
     * @param {string} val
     * @param {number} fixNum 保留小数位
     * @param {string} extra 单位或额外字段
     * return string val
     * */
    fixNumber: function(val, fixNum = 2, extra = '') {
        if (!isNaN(val) && val || val == 0) {
            return Number(val).toFixed(fixNum) + extra;
        }
        return val;
    }
};

// 注册全局过滤器
for (let key in filters) {
    Vue.filter(key, filters[key]);
}
