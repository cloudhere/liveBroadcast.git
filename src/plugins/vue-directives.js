/**
 * create by xujb 20180929 [自定义指令集合]
 */
import Vue from 'vue';
import clickoutclose from './directives/v-clickoutclose.js'; // 点击外部关闭事件
import tap from './directives/v-tap.js'; // 点击外部关闭事件

let arr = [clickoutclose, tap];

// 批量注册指令
arr.forEach(item => {
    Vue.directive(item.name, item.option);
});
