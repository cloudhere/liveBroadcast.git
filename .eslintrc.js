module.exports = {
    root: true,
    env: {
        node: true,
        browser: true,
        es6: true
    },
    extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
    parserOptions: {
        parser: 'babel-eslint'
    },
    rules: {
        // eslint 规则
        'no-console': process.env.NODE_ENV === 'production' ? 'off' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'off' : 'off',
        indent: ['off', 4, { SwitchCase: 1 }],
        quotes: [1, 'single'],
        'no-empty': 'off',
        // "semi": ["error", "always", { "omitLastInOneLineBlock": true}],
        // prettier 规则
        'prettier/prettier': [
            'off',
            {
                singleQuote: true,
                tabWidth: 4
            }
        ]
    }
};
